package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class JFLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		setTitle("SisRodoviaria - TELAdeLogin");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 733, 470);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("SISRODOVIARIA - BEM VINDO!");
		lblNewLabel.setFont(new Font("Steinberg", Font.PLAIN, 30));
		lblNewLabel.setBounds(147, 10, 438, 86);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Informe suas credenciais de acesso");
		lblNewLabel_1.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(169, 71, 420, 25);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Usuario: ");
		lblNewLabel_2.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 20));
		lblNewLabel_2.setBounds(199, 106, 99, 25);
		contentPane.add(lblNewLabel_2);
		
		txtUsuario = new JTextField();
		txtUsuario.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 15));
		txtUsuario.setBounds(289, 106, 198, 26);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Senha:");
		lblNewLabel_3.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 20));
		lblNewLabel_3.setBounds(199, 137, 76, 32);
		contentPane.add(lblNewLabel_3);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(289, 141, 198, 25);
		contentPane.add(txtSenha);
		
		JButton btnAcessar = new JButton("Acessar");
		btnAcessar.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 15));
		btnAcessar.setBounds(249, 179, 107, 32);
		contentPane.add(btnAcessar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 15));
		btnCancelar.setBounds(374, 179, 113, 32);
		contentPane.add(btnCancelar);
		
		JButton btnCadastrarse = new JButton("Cadastrar-se");
		btnCadastrarse.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 12));
		btnCadastrarse.setBounds(225, 231, 113, 32);
		contentPane.add(btnCadastrarse);
		
		JButton btnRecuperarsenha = new JButton("Recuperar senha");
		btnRecuperarsenha.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 12));
		btnRecuperarsenha.setBounds(340, 231, 147, 32);
		contentPane.add(btnRecuperarsenha);
	}
}
